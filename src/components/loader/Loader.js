import React from 'react';
import {View, Text, Image} from 'react-native';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
} from 'react-native-indicators';
import styles from './styles';

const Loader = (props) => {
    return (
        <View style={styles.container}>
            <Image style={{resizeMode: 'contain'}}
                   source={require('../../assets/logo/loading.png')}/>
            <View style={{flexDirection: 'row'}}>
                <View style={{width: '20%', height: 60}}>
                    <SkypeIndicator color='rgb(10, 209, 243)' size={50} style={{height: 90}} />
                </View>
                <Text style={[styles.text, {color: '#353b48', fontFamily: 'IRANSansMobile(FaNum)'}]}>{props.send ? 'در حال دریافت اطلاعات' : 'در حال دریافت اطلاعات'}</Text>
            </View>
        </View>
    );
};
Loader.defaultProps = {
    size: 'large',
    animating: true,
    color: '#353b48',
    send:true
};
export default Loader;

