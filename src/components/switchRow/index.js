import React, {Component} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import styles from './styles'
import SwitchButton from '../../components/switchButton'
import { Switch } from 'react-native-switch';

class SwitchRow extends Component {

    constructor(props){
        super(props);
        this.state = {
            switch: this.props.on
        };
    }
    render() {
        return (
            <View style={styles.reduction}>
                {/*<SwitchButton />*/}
                <Switch
                    value={this.state.switch}
                    // onValueChange={() => this.setState({switch: !this.state.switch})}
                    onValueChange={() => this.setState({switch: !this.state.switch}, (event) => {this.props.switchButtons(this.state.switch)})}
                    disabled={this.props.on}
                    activeText={'On'}
                    inActiveText={'Off'}
                    circleSize={20}
                    barHeight={20}
                    circleBorderWidth={1}
                    backgroundActive={'rgb(83, 215, 105)'}
                    backgroundInactive={'gray'}
                    circleActiveColor={'white'}
                    circleInActiveColor={'#000000'}
                    changeValueImmediately={true}
                    // renderInsideCircle={() => <CustomComponent />} // custom component to render inside the Switch circle (Text, Image, etc.)
                    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                    outerCircleStyle={{borderColor: 'rgb(83, 215, 105)' }} // style for outer animated circle
                    renderActiveText={false}
                    renderInActiveText={false}
                    switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                    switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                    switchWidthMultiplier={2} // multipled by the `circleSize` prop to calculate total width of the Switch
                />
                <Text style={styles.redlabel}>{this.props.label}</Text>
            </View>
        );
    }
}
export default SwitchRow;