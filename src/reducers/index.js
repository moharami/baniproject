import { combineReducers } from 'redux';
// import {nav} from './nav';
import auth from './auth';
import posts from './posts';

export default combineReducers({
    posts,
    auth
});

