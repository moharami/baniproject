import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import splash from '../../assets/splash.png'
import {
    Image,
    Dimensions
} from 'react-native';
import * as Animatable from 'react-native-animatable';


class Splash extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        setTimeout( () => {
            this.redirect();
        },4000);
    }
    redirect () {
        Actions.push('login');
    }
    render() {

        const  Winwidth = Dimensions.get('window').width;
        const  Winheight = Dimensions.get('window').height;
        return (
            <Animatable.Image
                animation="fadeOut"
                duration={6000}
                source={splash}
                style={{width: Winwidth, height: Winheight, resizeMode: 'contain' }}
                onError={() => this.setState({source: 'not_found.jpg'})}
            />
        )
    }
}

export default Splash;