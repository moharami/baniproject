
import React, {Component} from 'react';
import { View, TouchableOpacity, Text, AsyncStorage,ImageBackground, Dimensions, ScrollView, BackHandler, Alert} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios';
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import Order from "../../components/order";
import AlertView from '../../components/modalMassage'

class UserOrders extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: true,
            orders: [],
            modalVisible: false,
            showMore: true
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        if(this.props.profileBack) {
            Actions.pop({refresh: {refresh:Math.random()}});
        }
        else {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        }
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                Axios.post('/request/orders/list', {
                    user_id: newInfo.user_id
                }).then(response=> {
                    this.setState({loading: false, orders: response.data.data})
                    console.log('oooorders', this.state.orders)
                    console.log('oooorders', this.state.orders.length)
                })
                    .catch((error) => {
                        console.log(error)
                        // Alert.alert('','خطا');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    });
            }
            else {
                this.setState({loading: false});
            }

        });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else
        return (
            <View style={styles.container}>
                <View style={styles.image}>
                    <View style={styles.imageRow}>
                        <TouchableOpacity onPress={() => this.onBackPress()}>
                            <Icon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                        </TouchableOpacity>
                        <Text style={styles.label}>سفارش های من</Text>
                        <TouchableOpacity onPress={() => this.props.openDrawer()}>
                            <Icon name="menu" size={26} color="white" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.state.orders.length !== 0 ? this.state.orders.map((item, index) => <Order key={index} item={item} />) :
                                <Text style={{paddingTop: '50%'}}>سفارشی برای نمایش وجود ندارد</Text>
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setModalVisible(visible)}
                            //
                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer}/>
                <View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />
            </View>

        );
    }
}
export default UserOrders;