import React, {Component} from 'react';
import {View, TouchableOpacity, Text, TextInput, Image,BackHandler, AsyncStorage, KeyboardAvoidingView, NetInfo} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import AlertView from '../../components/modalMassage'
import moment_jalaali from 'moment-jalaali'

class Login extends Component {
    constructor(props) {
        super(props);
        this.backCount = 0;
        this.state = {
            text: '',
            login: true,
            loading: false,
            mobileCorrect: false,
            modalVisible: false,
            correct: false,
            isConnected: false,
            wifi: false,
        };
        // this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        // console.log('some thing unmount')
        BackHandler.removeEventListener("hardwareBackPress",  this.onBackPress);
        // BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        // console.log('some thing loged')
        // BackHandler.exitApp();
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    // componentWillUpdate(){
    //     BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    // }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
        // console.log('herer is login')
        // NetInfo.getConnectionInfo().then((connectionInfo) => {
        //     this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
        //         if(this.state.isConnected === false){
        //             this.setState({modalVisible: true, loading: false, wifi: true})
        //         }
        //     })
        // });
        // AsyncStorage.removeItem('token')
        // try {
        //     AsyncStorage.getItem('token').then((info) => {
        //         if(info !== null) {
        //             const newInfo = JSON.parse(info);
        //             // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
        //             console.log('comment token', newInfo.token);
        //             const expiresTime = newInfo.expires_at;
        //             const currentTime = new Date().getTime()/1000;
        //             if(expiresTime> currentTime ){
        //                 // Actions.home({openDrawer: this.props.openDrawer, loged: true})
        //                 Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        //                 this.setState({loading: false});
        //             }
        //             else {
        //                 Actions.login({openDrawer: this.props.openDrawer})
        //                 this.setState({loading: false});
        //                 // this.setState({info: true});
        //             }
        //         }
        //         else{
        //             this.setState({loading: false});
        //         }
        //     });
        // }
        // catch (error) {
        //     console.log(error)
        // }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    onLogin(){
        console.log(this.state.text)
        // console.log(/‎09[123]\d{8}/g.test('09125555555'))
        // if (reg.test("09124180623") === true){
        //     alert('true')
        //     this.setState({mobileCorrect: true})
        // }
        if(this.state.text === '' )
        {
            this.setState({ correct: true, modalVisible: true});
        }
            // Alert.alert('', 'لطفا شماره موبایل صحیح را وارد کنید');

        else{
            this.setState({loading: true});
            Axios.post('/send_login', {
                mobile: this.state.text
            }).then(response=> {
                this.setState({loading: false});
                console.log('send mobile', response.data);
                if(response.data.msg === 'CodeSendSuccess'){
                    this.setState({
                        loading: false
                    }, () => {
                        console.log('pppppprofile true in login', this.props.profile)
                        if (this.props.profile ===  true) {
                            Actions.confirmation({mobile: this.state.text, profile: true});
                        }
                        else {
                            Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                        }
                    })
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, profile: true})
                }
            })
            .catch((response) => {
                console.log('response error', response.response)
                if(response.response.data.msg === 'NotRegister'){
                    this.setState({
                        loading: false
                    }, () => {
                        Axios.post('/send_register', {
                            mobile: this.state.text
                        }).then(response=> {
                            if(response.data.msg === 'Register'){
                                console.log('register success', response.data);
                                this.setState({
                                    loading: false
                                }, () => {
                                    if (this.props.profile ===  true) {
                                        Actions.confirmation({mobile: this.state.text, profile: true});
                                    }
                                    else {
                                        Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                                    }

                                    // Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                                })
                            }
                            if(response.data.msg === 'BeforeRegister'){
                                this.setState({loading: false})
                            }
                        })
                        .catch((error) => {
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                        // Actions.confirmation({mobile: this.state.text});
                    })
                }
                else if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'CodeSendError'){
                    this.setState({
                        loading: false
                    }, () => {
                        // Alert.alert('','لطفا شماره موبایل خود را صحیح وارد کنید');
                        // this.setState({correct: true, loading: false});
                        this.setState({correct: true, modalVisible: true, loading: false});

                        // Actions.login({mobile: this.state.text});
                    })
                }
                else {
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                }
            });
        }
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <KeyboardAvoidingView style={styles.container}  behavior="padding" enabled>
                {this.state.loading ? <Loader send={false}/> :
                    <View style={styles.send}>
                        <Image style={{alignSelf: 'center', marginRight: 20, width: '40%', resizeMode: 'contain'}} resizeMode={'contain'}
                               source={require('../../assets/logo/Logo.jpg')}/>
                        <View style={styles.body}>
                            <Text style={styles.header}>خوش آمدید</Text>
                            <Text style={styles.label}>برای بهره مندی از خدمات بانی بیمه لطفا وارد شوید</Text>
                            <View style={{display: 'flex', flexDirection: 'row'}}>
                                <TextInput
                                    maxLength={11}
                                    placeholder="شماره موبایل"
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text}
                                    style={{
                                        textAlign: 'right',
                                        borderWidth: .4,
                                        borderColor: '#C8C8C8',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 15,
                                        flex: .9,
                                        borderTopLeftRadius: 6,
                                        borderBottomLeftRadius: 6,
                                        borderBottomRightRadius: .1,
                                        borderTopRightRadius: 1,
                                        fontSize: 18,
                                        color: '#7A8299',
                                        fontFamily: 'IRANSansMobile(FaNum)'
                                    }}
                                    onChangeText={(text) => this.setState({text})}/>
                                <View style={{
                                    backgroundColor: '#C8C8C8',
                                    borderTopRightRadius: 6,
                                    borderBottomRightRadius: 6,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 45
                                }}>
                                    <Icon name={'mobile-phone'}
                                          style={{fontSize: 30, paddingRight: 15, paddingLeft: 15}}/>
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => this.onLogin() } style={styles.advertise}>
                                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3CB1E8', '#3E40DB']}
                                                style={styles.advertise}>
                                    <View>
                                        <Text style={styles.buttonTitle}>ورود</Text>
                                    </View>
                                </LinearGradient>
                            </TouchableOpacity>
                            {/*<View style={styles.footer}>*/}
                                {/*<TouchableOpacity onPress={() => Actions.push('register')}>*/}
                                    {/*<Text style={styles.signup}>ثبت نام کنید</Text>*/}
                                {/*</TouchableOpacity>*/}
                                {/*<Text style={styles.footerText}>کاربر جدید هستید؟ / </Text>*/}
                            {/*</View>*/}
                        </View>
                    </View>
                }
                <Image style={{height:'20%',zIndex:-1,width:'100%',position:'absolute',bottom:0}} resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    // onChange={(visible) => this.setModalVisible(visible)}

                    title={this.state.correct ? 'لطفا شماره موبایل خود را صحیح وارد کنید':(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                />
            </KeyboardAvoidingView>
        );
    }
}

export default Login;