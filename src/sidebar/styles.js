import { StyleSheet, Dimensions } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: '#353b48',
        height: '100%'
    },
    container: {
        // backgroundColor: 'rgba(17, 103, 253, 1)',
        backgroundColor: '#353b48',
        alignItems: 'flex-start',
        justifyContent: 'center',
        // height: Dimensions.get('window').height
    },
    profile: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 15,
        paddingBottom: 40
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 100,
        elevation: 8
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 50
    },
    name: {
        color: 'white',
        fontSize: 16,
        paddingRight: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
    }
});
